#include "Astar.h"


Astar::Astar()
{
	m_directions[0] = std::pair<int, int>{ 0, 1 };	// Up
	m_directions[1] = std::pair<int, int>{ 0, -1 };	// Down
	m_directions[2] = std::pair<int, int>{ -1, 0 };	// Left
	m_directions[3] = std::pair<int, int>{ 1, 0 };	// Right
}


Astar::~Astar()
{
}


// Compares Nodes total cost to determine if left node is less than the right node
bool compareNodes(Node lhs, Node rhs)
{
	return (lhs.getF() < rhs.getF());
}


// Compares Nodes G cost to determine if left node is more than the right node
// This gives a reverse order path ie ( startnode = closed.back() )
bool compareG(Node lhs, Node rhs)
{
	return (lhs.getG() > rhs.getG());
}


bool Astar::findPath(Node &startNode, Node &targetNode, int &size, std::vector<Node> &map, std::vector<std::vector<int>> &adjMat)
{
	std::pair<int, int> pos;

	m_path.clear();

	m_foundGoal = false;
	m_curCost = 0;

	int sizeX = size;

	int nodeIndex = 0;

	StartTimer = SDL_GetTicks();

	Node curNode = startNode;	// Set currentNode
	Node neighbourNode;

	// Calculate costs (F, G, H)
	calculateH(curNode, targetNode);	// H
	calculateF(curNode);				// F

	m_openNodes.push_back(curNode);	// Add CurrentNode to list of open nodes

	while (!m_openNodes.empty())	// While there are still possible Nodes to explore
	{
		// Sort openNodes by total cost (Lowest F is at the front)
		std::sort(m_openNodes.begin(), m_openNodes.end(), compareNodes);
		// Reverse sorted openNodes (Lowest F now at the back)
		std::reverse(m_openNodes.begin(), m_openNodes.end());

		curNode = m_openNodes.back();		// Current node is the node in the openNodes with the lowest F
		m_openNodes.pop_back();				// Pop current node from list of open nodes
		m_closedNodes.push_back(curNode);	// Add current node to list of closed nodes

		nodesSearched++;

		if (curNode.getPos() == targetNode.getPos())
		{
 			m_foundGoal = true;

			// Remove irrelavent nodes
			findParent(map, adjMat);

			// Sort Nodes
			std::sort(m_closedNodes.begin(), m_closedNodes.end(), compareG);

			break;	// Path has been found so exit loop
		}

		for (unsigned int j = 0; j < map.size(); ++j)
		{
			if (map[j].getPos() == curNode.getPos())
			{
				nodeIndex = j;
				break;
			}
		}

		for (unsigned int i = 0; i < map.size(); ++i)
		{
			if (adjMat[i][nodeIndex] != 0)
			{
				neighbourNode = map.at(i);
				isValidNeighbour(curNode, neighbourNode, targetNode);
			}
		}

		m_curCost = curNode.getG();	// Get Current Cost (G)

		if (m_openNodes.empty())
		{
			m_foundGoal = false;
		}
	}

	if (m_foundGoal)
	{
		while (m_closedNodes.empty() == false)
		{
			//Reset Node values
			m_closedNodes.back().setG(0);
			m_closedNodes.back().setH(0.0f);
			m_closedNodes.back().setF(0.0f);
			Node temp = m_closedNodes.back();	// Set temp to equal back of closedNode
			m_path.push_back(temp.getPos());	// Add node to path
			m_closedNodes.pop_back();			// Remove back of closedNode
		}
	}

	while (m_openNodes.empty() == false)
	{
		//Reset Node values
		m_openNodes.back().setG(0);
		m_openNodes.back().setH(0.0f);
		m_openNodes.back().setF(0.0f);

		// Remove waypoint from openNodes
		m_openNodes.pop_back();
	}

	printf("Nodes searched: %d\n", nodesSearched);

	endTimer = SDL_GetTicks();

	time = endTimer - StartTimer;

	float timeSec = float(time) / 1000.0f;

	printf("Time Taken: %d ms, %d s\n", time, timeSec);

	return m_foundGoal;
}


std::vector< std::pair<int, int> > Astar::getPath()
{
	return m_path;
}


void Astar::printPath()
{
	for (unsigned int i = 0; i < m_path.size(); ++i)
	{
		// cant be bothered including iostream for one print to console
		printf("[%d] x: %d, y: %d\n" , i, m_path.at(i).first, m_path.at(i).second);
	}
}


void Astar::calculateG(Node &currentNode)
{
	// Current node cost (G) = current node weight + previous total current cost
	currentNode.setG((currentNode.getWeight() + m_curCost));	// G
}


void Astar::calculateH(Node &currentNode, Node &targetNode)
{
	// Calculates the Euclidean distance between current and target nodes
	// Then calculates the cost to travel from current to target nodes
	float dx = (float)abs(currentNode.getPos().first - targetNode.getPos().first);
	float dy = (float)abs(currentNode.getPos().second - targetNode.getPos().second);

	currentNode.setH(sqrt(dx * dx + dy * dy));	// H
}


void Astar::calculateF(Node &currentNode)
{
	// F = G + H
	currentNode.setF((currentNode.getG() + currentNode.getH()));	// F
}


void Astar::isValidNeighbour(Node &currentNode, Node &neighbourNode, Node &targetNode)
{
	bool isInOpenNodes = false;
	bool isInClosedNodes = false;

	if (neighbourNode.getWeight() != 0)	// if cost is zero node is not accessible
	{
		size_t index;
		for (index = 0; index < m_openNodes.size(); ++index)	// Is neighborNode in openNodes
		{
			// if neighbor node is in open set
			if (neighbourNode.getPos() == m_openNodes[index].getPos())
			{
				// current cost to neighbor < cached cost to neighbor)
				if (currentNode.getG() < neighbourNode.getG())
				{
					// Move node to back of the vector then remove the node from the vector
					std::swap(m_openNodes[index], m_openNodes.back());
					m_openNodes.pop_back();	// remove neighbor from closed
				}

				isInOpenNodes = true;
				break;
			}
		}
		for (index = 0; index < m_closedNodes.size(); ++index)	// Is currentNode in closedNodes
		{
			// if neighbor node is in closed set
			if (neighbourNode.getPos() == m_closedNodes[index].getPos())
			{
				// current cost to neighbor < cached cost to neighbor
				if (currentNode.getG() < neighbourNode.getG())
				{
					// Move node to back of the vector then remove the node from the vector
					std::swap(m_closedNodes[index], m_closedNodes.back());
					m_closedNodes.pop_back();	// remove neighbor from closed
				}

				isInClosedNodes = true;
				break;
			}
		}
		if (isInClosedNodes == false && isInOpenNodes == false)
		{
			// Calculate costs (F, G, H)
			calculateG(neighbourNode);				// G
			calculateH(neighbourNode, targetNode);	// H
			calculateF(neighbourNode);				// F

			m_openNodes.push_back(neighbourNode);	// add currentNode to openNodes
		}
	}

}



void Astar::findParent(std::vector<Node> &map, std::vector<std::vector<int>> &adjMat)
{
	bool isParent = false;

	int index = 0, conIndex = 0;

	std::vector<int> parents;

	conIndex = m_closedNodes.size() - 1;

	while (conIndex > 1)
	{

		for (unsigned int i = 0; i < map.size(); ++i)
		{
			if (m_closedNodes[conIndex].getPos() == map[i].getPos())
			{
				index = i;
				break;
			}
		}

		for (unsigned int j = 0; j < map.size(); ++j)
		{
			if (adjMat[j][index] != 0)
			{
				parents.push_back(j);
			}

			if (parents.size() == 4)
			{
				break;
			}
		}

		while (isParent == false)
		{
			isParent = false;

			for (unsigned int p = 0; p < parents.size(); ++p)
			{
				if (m_closedNodes[conIndex-1].getPos() == map[parents[p]].getPos())
				{
					isParent = true;
					break;
				}
			}

			// Remove Node due to backtracking
			if (isParent == false)
			{
				// Set node to back then pop back
				std::swap(m_closedNodes[conIndex-1], m_closedNodes.back());
				m_closedNodes.pop_back();
				--conIndex;
			}
		}

		isParent = false;

		parents.clear();

		--conIndex;
	}

}