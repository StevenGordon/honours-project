#pragma once

#include <memory>
#include <array>
#include <queue>
#include <SDL.h>

#include "../Map/Node.h"

class Astar
{
public:
	Astar();
	~Astar();

	// Calculates the path form start to target with the lowest cost and returns true if a path is found
	bool findPath(Node &startNode, Node &targetNode, int &size, std::vector<Node> &map, std::vector<std::vector<int>> &adjMat);

	// Return the path for the object using pathfinding
	std::vector< std::pair<int,int> > getPath();

	// Displays path on console
	void printPath();

private:

	// Calculates the cost from start by incrementing current cost with this nodes weight	
	void calculateG(Node &currentNode);

	// Calculates the cost to the target by calculating the distance from current node to target	
	void calculateH(Node &currentNode, Node &targetNode);

	// Adds cost from start(G) and heuristic cost(H) together to get the total cost(F) -> F = G + H
	void calculateF(Node &currentNode);

	// Checks is the neighbour node valid (ie can it be added to the open list)
	void isValidNeighbour(Node &currentNode, Node &neighbourNode, Node &targetNode);

	// Removes unnessesary nodes from closed list
	void findParent(std::vector<Node> &map, std::vector<std::vector<int>> &adjMat);

	enum Dir { up, down, left, right };

	std::pair<int, int> m_directions[4];

	// Possible node the path can progress
	std::vector<Node> m_openNodes;

	// Lowest total cost nodes (the path in incorrect order)
	std::vector<Node> m_closedNodes;

	// The resulting path
	std::vector< std::pair<int,int> > m_path;

	int m_curCost;	// cost from start tile to current tile
	std::pair<int,int> m_nextNode;	// The next position an object should move to

	bool m_foundGoal;	// Has a path been found

	int nodesSearched = 0;

	// Timer functions
	int StartTimer;
	int endTimer;
	int time;
};



