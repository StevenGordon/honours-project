#include "Ant.h"


Ant::Ant()
{
	this->m_pCost = 0;
}


Ant::~Ant()
{
	m_path.clear();
}


void Ant::setPath(std::vector<int> path)
{
	this->m_path = path;
}


void Ant::setPath(int value)
{
	this->m_path.push_back(value);
}


void Ant::resetPath()
{
	this->m_path.clear();
}


std::vector<int> Ant::getPath()
{
	return this->m_path;
}



void Ant::setPCost(int cost)
{
	this->m_pCost = cost;
}


int Ant::getPCost()
{
	return this->m_pCost;
}


void Ant::resetAnt()
{
	resetPath();
	setPCost(0);
}