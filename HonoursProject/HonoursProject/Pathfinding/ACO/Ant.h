#pragma once

#include <vector>

class Ant
{
public:
	Ant();
	~Ant();

	// Set whole Path
	void setPath(std::vector<int> path);

	// Add value on the Path
	void setPath(int value);

	// Clears the ants path
	void resetPath();

	std::vector<int> getPath();

	void setPCost(int cost);
	int getPCost();

	void resetAnt();

private:
	std::vector<int> m_path;
	
	// The total cost of the path
	int m_pCost;
};

