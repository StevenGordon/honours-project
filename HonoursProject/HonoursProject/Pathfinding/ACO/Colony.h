#pragma once

#include <vector>
#include <random>
#include <SDL.h>

#include "Ant.h"

class Colony
{
public:
	Colony();
	~Colony();

	bool ACO(int start, int goal, int ants, std::vector<std::vector<int>> &adjMat);

	void initACO(int ants, std::vector<std::vector<int>> &adjMat);

	// Initialise the pheromone level of every node
	void initPheromes(int numNodes);

	// Apply the evaporation rate to the phermone to the nodes
	void evapP();

	// Update the pheromones
	void updP(unsigned int &a);

	// Ant lays pheromone on to node reinforcing the pheromone level
	// This is not to be used with updP!
	void reinfP(Ant &a);

	// Calculate Ant a's path
	void findPath(unsigned int &ant, std::vector<std::vector<int>> &adjMat);

	// Finds the next node from the neighbours of the current node
	int nextNode(unsigned int &ant, unsigned int &a, std::vector<std::vector<int>> &adjMat);

	// Calculates the probability the ant will travel to this node
	double calProb(unsigned int &a, int &b, std::vector<std::vector<int>> &adjMat);

	int calCost(unsigned int &a, std::vector<std::vector<int>> &adjMat);

	// Returns the best path
	std::vector<int> getPath();

private:
	// Heuristic function which uses cost from adjacency Matrix as distance
	float distance(unsigned int &a, int &b, std::vector<std::vector<int>> &adjMat);

	void nextNode(Ant &a);

	// The number of ants in the colony
	int m_colonySize;

	// Vector of the ants in the colony
	std::vector<Ant> m_ants;

	// Copy of the best ant so far
	Ant m_bAnt;

	// Vector of pheromone levels for each node
	std::vector<float> m_pheromone;

	// Evaporation rate
	float m_rho = 0.01f;

	// Pheromone level that an ant deposits on a node
	float m_weight = 2.0f;

	// Higher alpha favours pheromone over heuristic
	int alpha = 3;

	// Higer beta favour heuristic over pheromone
	int beta = 2;

	// Proability of selecting a node
	float m_prob;
	
	// Heuristic value
	float m_dist;

	// Number of nodes in maze
	int m_noNodes;

	int m_start, m_goal;

	int m_maxAnts = 500;

	int m_maxEpochs = 500;

	bool m_pathFound;

	int nodesSearched = 0;

	// Timer functions
	int StartTimer;
	int endTimer;
	int time;
};

