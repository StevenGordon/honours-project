#include "Colony.h"


Colony::Colony()
{
}


Colony::~Colony()
{
	m_pheromone.clear();

	m_ants.clear();
}


bool Colony::ACO(int start, int goal, int ants, std::vector<std::vector<int>> &adjMat)
{
	m_pathFound = false;
	int epochs = 0;
	unsigned int nAnts;			// number of ants

	StartTimer = SDL_GetTicks();

	m_start = start;
	m_goal = goal;

	initACO(ants, adjMat);

	m_bAnt = m_ants[0];
	m_bAnt.setPCost(INT_MAX);

	while (epochs < m_maxEpochs)
	{
		// Calculate ants path and cost
		for (nAnts = 0; nAnts < m_ants.size(); ++nAnts)
		{
			findPath(nAnts, adjMat);
			if (m_ants[nAnts].getPCost() != INT_MAX)
			{
				m_ants[nAnts].setPCost(calCost(nAnts, adjMat));
			}
		}

		// compare ants with best ant
		for (nAnts = 0; nAnts < m_ants.size(); ++nAnts)
		{
			if (m_ants[nAnts].getPCost() < m_bAnt.getPCost())
			{
				m_bAnt = m_ants[nAnts];
			}
		}

		evapP();

		for (nAnts = 0; nAnts < m_ants.size(); ++nAnts)
		{
			if (m_ants[nAnts].getPCost() != INT_MAX)
			{
				updP(nAnts);
			}
		}

		for (nAnts = 0; nAnts < m_ants.size(); ++nAnts)
		{
			m_ants[nAnts].getPath().clear();
		}

		epochs++;
	}

	printf("Nodes searched: %d\n", nodesSearched);

	endTimer = SDL_GetTicks();

	time = endTimer - StartTimer;

	float timeSec = float(time) / 1000.0f;

	printf("Time Taken: %d ms, %d s", time, timeSec);

	return m_pathFound;
}


void Colony::initACO(int ants, std::vector<std::vector<int>> &adjMat)
{
	m_noNodes = adjMat.size();

	if (ants < m_maxAnts)
	{
		m_ants.resize(ants);
		m_colonySize = m_ants.size();
	}
	else
	{
		m_ants.resize(m_maxAnts);
		m_colonySize = m_ants.size();
	}

	for (unsigned int i = 0; i < m_ants.size(); ++i)
	{
		m_ants[i].setPath(m_start);
	}

	initPheromes(m_noNodes);
}


void Colony::initPheromes(int numNodes)
{
	float initPherLvl = 0.15f;
	//float initPherLvl = 0.01f;

	m_pheromone.resize(numNodes, initPherLvl);
}


void Colony::evapP()
{
	for (unsigned int i = 0; i < m_pheromone.size(); ++i)
	{
		m_pheromone[i] -= m_rho;
	}
}


void Colony::updP(unsigned int &a)
{
	int index = 0;
	unsigned int length = m_ants[a].getPath().size();
	float pherLvl = 1.0f / length;

	for (unsigned int i = 0; i < length; ++i)
	{
		index = m_ants[a].getPath()[i];
		m_pheromone[index] += m_weight;
	}
}


void Colony::reinfP(Ant &a)
{
	m_pheromone[a.getPath().back()] += m_weight;
}


float Colony::distance(unsigned int &a, int &b, std::vector<std::vector<int>> &adjMat)
{
	int dist = adjMat[a][b];

	return 1.0f / dist + 0.1f;
}


// Calculate Ant a's path
void Colony::findPath(unsigned int &ant, std::vector<std::vector<int>> &adjMat)
{
	unsigned int curNode = m_start;
	unsigned int newNode = 0;

	// Find a path or reach max length of a path
	while (m_ants[ant].getPath().size() < adjMat.size())
	{
		// Calculate the next Node from the current Node
		newNode = nextNode(ant, curNode, adjMat);

		if (newNode == INT_MAX)
		{
			m_ants[ant].setPCost(INT_MAX);
			break;
		}
		nodesSearched++;

		// Add node to path
		m_ants[ant].setPath(newNode);
		// Set node to current the Node
		curNode = newNode;

		if (curNode == m_goal)
		{
			m_pathFound = true;
			break;
		}
	}

}


int Colony::nextNode(unsigned int &ant, unsigned int &a, std::vector<std::vector<int>> &adjMat)
{
	std::vector<int> neighbours;
	std::vector<float> probs;
	std::vector<float> cumul;
	float denom = 0.0f;
	int index = 0;
	unsigned int i;
	unsigned int j;
	bool visited = false;
	int nextNode = 0;

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<float> dis(0.0f, 1.0f);

	// find connected nodes
	for (i = 0; i < adjMat.size(); ++i)
	{
		if (adjMat[a][i] != 0)
		{
			if (m_ants[ant].getPath().size() > 1)
			{
				visited = false;
				for (j = 0; j < m_ants[ant].getPath().size(); ++j)
				{
					if (m_ants[ant].getPath()[j] == i)
					{
						visited = true;
					}
				}
				if (visited == false)
				{
					neighbours.push_back(i);
				}
			}
			else
			{
				neighbours.push_back(i);
			}
		}
		if (neighbours.size() == 3)
		{
			break;
		}
	}

	if (neighbours.size() > 0)
	{
		if (neighbours.size() == 1)
		{
			return neighbours[0];
		}

		cumul.resize(neighbours.size()+1);

		for (i = 0; i < neighbours.size(); ++i)
		{
			denom += calProb(a, neighbours[i], adjMat);
		}
		for (i = 0; i < neighbours.size(); ++i)
		{
			probs.push_back(calProb(a, neighbours[index], adjMat) / denom);
		}

		if (probs.size() > 1)
		{
			for (i = 0; i < neighbours.size(); ++i)
			{
				cumul[i+1] = cumul[i] + probs[i];
			}
			do
			{
				m_prob = dis(gen);

				for (i = 0; i < probs.size(); ++i)
				{
					nextNode = i + 1;
					if (nextNode == probs.size())
					{
						nextNode = 0;
					}
					if (m_prob >= cumul[i] && m_prob < cumul[i+1])
					{
						return  neighbours[i];
					}
				}
			} while (1);
		}
	}
	else
	{
		return INT_MAX;
	}

	// return node index
	return neighbours[index];
}


double Colony::calProb(unsigned int &a, int &b, std::vector<std::vector<int>> &adjMat)
{
	// p = pher^alpha * 1/dist^beta
	return std::pow(m_pheromone[a], alpha) * std::pow(distance(a,b, adjMat), beta);
}


int Colony::calCost(unsigned int &a, std::vector<std::vector<int>> &adjMat)
{
	int curNode = m_start;
	int total = 0;
	unsigned int index = 0;

	for (index = 1; index < m_ants[a].getPath().size(); ++index)
	{
		total += adjMat[curNode][m_ants[a].getPath()[index]];
		
		curNode = m_ants[a].getPath()[index];
	}

	return total;
}


std::vector<int> Colony::getPath()
{
	return m_bAnt.getPath();
}