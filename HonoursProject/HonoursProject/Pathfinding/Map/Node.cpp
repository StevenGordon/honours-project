#include "Node.h"


Node::Node()
{
	m_x, m_y, m_c, m_w, m_g, m_h, m_f = 0;
}


Node::Node(int iX, int iY, int cost) : m_x(iX), m_y(iY), m_c(cost)
{
	m_w = m_c;
	m_g = 0;
	m_h = 0;
	m_f = 0;
}


Node::~Node()
{

}


unsigned int Node::getCost()
{
	return m_c;
}


unsigned int Node::getWeight()
{
	return m_w;
}


void Node::setWeight(unsigned int weight)
{
	m_w += weight;
}


unsigned int Node::getG()
{
	return m_g;
}

void Node::setG(int costFromStart)
{
	m_g = costFromStart;
}


float Node::getH()
{
	return m_h;
}


void Node::setH(float heuristicCost)
{
	m_h = heuristicCost;
}


float Node::getF()
{
	return m_f;
}


void Node::setF(float totalCost)
{
	m_f = totalCost;
}