#pragma once

#include <iostream>

#include "Node.h"
#include <vector>

class VisibilityGraph
{
public:
	VisibilityGraph(int isizeX, int isizeY, const int imaxX,  const int imaxY);

	~VisibilityGraph();

	void calculateCost(Node node1, Node node2);

private:
	int m_sizeX;
	int m_sizeY;

	const int m_maxSizeX;
	const int m_maxSizeY;

	// Adjacency matrix
	// Each row/column represents all of the nodes connected to this Node
	// eg graph[x] is Node x
	// The values are the cost of traversal from this node to the other
	// eg graph[x][y] = 3 then the traversal cost of Nodes x to y is 3
	// If the value is zero then the nodes are not connected
	std::vector<std::vector<int>> m_graph;
};

