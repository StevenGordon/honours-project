#include "Grid.h"


Grid::Grid()
{
}


Grid::~Grid()
{
}


void Grid::setGrid(std::vector<std::vector<int>> &grid)
{
	unsigned int x, y, nodeNo = 0;
	int cost = 0, size = 0;

	size = grid.size()*grid.size();

	// set size of list of Nodes
	m_grid.resize(size);

	for (y = 0; y < grid.size(); ++y)
	{
		for (x = 0; x < grid.at(0).size(); ++x)
		{
			cost = grid.at(x).at(y);

			if (cost != 0)
			{
				cost *= 2;

				Node temp = Node(x, y, cost);

				m_grid.at(nodeNo) = temp;

				++nodeNo;
			}
		}
	}

	// Resize m_grid to contain only valid nodes ie non empty Nodes
	m_grid.resize(nodeNo);

	// set size of adjacency matrix
	m_adjMatrix.resize(m_grid.size());

	for (unsigned int i = 0; i < m_adjMatrix.size(); ++i)
	{
		m_adjMatrix.at(i).resize(m_adjMatrix.size(), 0);
	}

	printf("No. of Nodes: %d \n", m_grid.size());

	setAdjMat(grid);

}


void Grid::setAdjMat(std::vector<std::vector<int>> &grid)
{
	unsigned int i, j, n = 0;
	std::pair<int, int> directions[4] = { { 0, 1 }, { 0, -1 }, { 1, 0 }, { -1, 0 } };
	std::pair<int, int> pos;
	int cost = 0;

	for (i = 0; i < m_grid.size(); ++i)
	{
		for (j = 0; j < 4; ++j)
		{
			pos = { directions[j].first + m_grid.at(i).getPos().first, directions[j].second + m_grid.at(i).getPos().second };

			if ((pos.first >= 0 && pos.first < grid.at(0).size()) && (pos.second >= 0 && pos.second < grid.size()))
			{
				cost = grid.at(pos.first).at(pos.second);

				if (cost != 0)
				{
					// TODO: Find a more efficient method of doing this
					for (n = 0; n < m_grid.size(); ++n)
					{
						if (pos == m_grid[n].getPos())
						{
							cost = 2; // all costs are 2
							m_adjMatrix[i][n] = cost;
						}
					}
				}
			}
		}
	}
}