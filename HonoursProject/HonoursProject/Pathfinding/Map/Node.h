#pragma once

#include <utility>

class Node
{
public:
	Node();
	Node(int iX, int iY, int cost);

	~Node();

	std::pair<int, int> getPos() { return std::pair < int, int > {m_x, m_y}; }

	unsigned int getCost();

	unsigned int getWeight();
	void setWeight(unsigned int weight);

	unsigned int getG();
	void setG(int costFromStart);

	float getH();
	void setH(float heuristicCost);

	float getF();
	void setF(float totalCost);

	const bool Node::operator< (const Node& rhs) { return ( this->m_f < rhs.m_f ); }

private:
	int m_x;
	int m_y;

	unsigned int m_w;
	unsigned int m_c;

	int m_g;	// G - cost from start
	float m_h;	// H - heuristic cost
	float m_f;	// F - total cost
};

