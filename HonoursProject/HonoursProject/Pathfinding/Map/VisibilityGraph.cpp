#include "VisibilityGraph.h"


VisibilityGraph::VisibilityGraph(int isizeX, int isizeY, const int imaxX, const int imaxY)
	: m_sizeX(isizeX), m_sizeY(isizeY), m_maxSizeX(imaxX), m_maxSizeY(imaxY)
{
	if (m_sizeX > m_maxSizeX)
	{
		m_sizeX = m_maxSizeX;
		std::cout << "x size greater than max!" << std::endl;
	}

	if (m_sizeY > m_maxSizeY)
	{
		m_sizeY = m_maxSizeY;
		std::cout << "y size greater than max!" << std::endl;
	}

	m_graph.resize(m_sizeX, std::vector<int>(m_sizeY));

	std::cout << "Graph Constructed" << std::endl;
}


VisibilityGraph::~VisibilityGraph()
{
	// TODO: Clear vector


	std::cout << "Graph Deconstructed" << std::endl;
}
