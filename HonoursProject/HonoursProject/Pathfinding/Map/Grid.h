#pragma once

#include "Node.h"

#include <vector>

class Grid
{
public:
	Grid();
	~Grid();

	void setGrid(std::vector<std::vector<int>> &grid);

	std::vector<Node> &getGrid() { return m_grid; }

	void setAdjMat(std::vector<std::vector<int>> &grid);

	std::vector<std::vector<int>> getAdjMat() { return m_adjMatrix; }

private:
	std::vector<Node> m_grid;

	// Matrix that contains the cost of traversing from one node to another valid node
	std::vector <std::vector<int>> m_adjMatrix;
};

