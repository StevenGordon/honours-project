#include "Pathfinding.h"


Pathfinding::Pathfinding()
{
	m_grid = Grid();

	m_aStar = Astar();

	m_pso = new Swarm();

	m_aco = Colony();
}


Pathfinding::~Pathfinding()
{
	delete m_pso;
	m_pso = nullptr;
}


bool Pathfinding::findPath(Node &start, Node &goal, int type)
{
	bool pathFound = false;

	int invalidIndex = 1000000;

	int indexStart = invalidIndex, indexGoal = invalidIndex;

	for (unsigned int i = 0; i < m_grid.getGrid().size(); ++i)
	{
		if (start.getPos() == m_grid.getGrid().at(i).getPos())
		{
			indexStart = i;
		}
		if (goal.getPos() == m_grid.getGrid().at(i).getPos())
		{
			indexGoal = i;
		}
		if (indexStart != invalidIndex && indexGoal != invalidIndex)
		{
			break;
		}
	}

	switch (type)
	{
	case 1:	// A*
		pathFound = this->m_aStar.findPath(start, goal, m_sizeX, m_grid.getGrid(), m_grid.getAdjMat());
		break;
	case 2:	// ACO
		pathFound = this->m_aco.ACO(indexStart, indexGoal, 10, m_grid.getAdjMat());
		break;
	case 3:	// PSO
		pathFound = this->m_pso->findPath(indexStart, indexGoal, m_grid.getAdjMat());
		break;
	default:
		// A*
		printf("Unrecognised selection using A*");
		pathFound = this->m_aStar.findPath(start, goal, m_sizeX, m_grid.getGrid(), m_grid.getAdjMat());
		break;
	}

	return pathFound;
}


std::vector<std::pair<int, int>> Pathfinding::getPath(int type)
{
	std::vector<std::pair<int, int>> path;
	std::vector<int> nodePath;

	switch (type)
	{
	case 1:	// A*
		path = m_aStar.getPath();
		break;
	case 2:	// ACO
		nodePath = m_aco.getPath();
		break;
	case 3:	// PSO
		nodePath = m_pso->getPath();
		break;
	default:
		printf("Unrecognised selection");
		break;

		if (path.empty() == true)
		{
			printf("Path is empty!");
		}

	}

	// If nodePath is not empty convert nodePath to path
	if (path.size() == 0 && nodePath.size() != 0)
	{
		for (unsigned int i = 0; i < nodePath.size(); ++i)
		{
			// Get Node.pos from the nodePath
			path.push_back(m_grid.getGrid()[ nodePath[i] ].getPos());
		}
	}

	return path;
}


void Pathfinding::setGrid(std::vector<std::vector<int>> map)
{
	m_sizeX = map.size();

	m_grid.setGrid(map);
}