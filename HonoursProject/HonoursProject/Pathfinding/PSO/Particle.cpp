#include "Particle.h"


Particle::Particle()
{
	mpBest = 0;
}


Particle::~Particle()
{
	mData.clear();
}


void Particle::init(int maxInputs)
{
	this->m_maxInputs = maxInputs;
	mData.resize(m_maxInputs);
	mVelocity.resize(m_maxInputs);
}


int Particle::getData(int index) const
{
	return this->mData[index];
}


void Particle::setData(int index, int value)
{
	this->mData[index] = value;
}


int Particle::getpBest() const
{
	return this->mpBest;
}


void Particle::setpBest(int value)
{
	this->mpBest = value;
}


float Particle::getVelocity(int index) const
{
	return this->mVelocity[index];
}


void Particle::setVelocity(int index, float value)
{
	this->mVelocity[index] = value;
}


std::vector<int> Particle::getPath() const
{
	return this->pPath;
}


void Particle::setPath(int value)
{
	this->pPath.push_back(value);
}


void Particle::clearPath()
{
	this->pPath.clear();
}


std::vector<int> Particle::getPBData() const
{
	return this->pbData;
}


void Particle::setPBData()
{
	// Clear current personal best Path
	this->pbData.clear();
	// Set personal best path to current path
	this->pbData = this->mData;
}


int Particle::getCost() const
{
	return this->pCost;
}


void Particle::setCost(int value)
{
	this->pCost = value;
}