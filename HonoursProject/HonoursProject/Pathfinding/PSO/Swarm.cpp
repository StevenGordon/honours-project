#include "Swarm.h"

/*
Swarm::Swarm()
{
	std::cout << "PSO Constructed" << std::endl;
}
*/

Swarm::~Swarm()
{
	bestData.clear();
	bestPath.clear();
	m_particles.clear();
}


bool Swarm::findPath(int startNode, int goalNode, std::vector<std::vector<int>> &adjMat)
{
	pathfound = false;

	START = startNode;

	TARGET = goalNode;

	srand((unsigned)time(0));

	StartTimer = SDL_GetTicks();

	psoAlgorithm(adjMat);

	std::cout << MAX_EPOCHS << " epochs completed." << std::endl;

	std::cout << "Nodes searched: " << nodesSearched << std::endl;

	endTimer = SDL_GetTicks();

	elapsedtime = endTimer - StartTimer;

	float timeSec = float(elapsedtime) / 1000.0f;

	printf("Time Taken: %d ms, %d s\n", time, timeSec);

	return pathfound;
}


void Swarm::psoAlgorithm(std::vector<std::vector<int>> &adjMat)
{
	int gBestTest = 0;
	int epoch = 0;
	bool done = false;
	int test = 0;

	initialize(adjMat);

	nBest = MAX_INPUTS;

	do
	{
		/*
			Two conditions can end this loop:
			if the maximum number of epochs allowed has been reached, or,
			if the Target value has been found.
		*/
		if (epoch < MAX_EPOCHS)
		{
			for (int i = 0; i < MAX_PARTICLES; i++)
			{
				if (m_particles[i].getCost() > 0)
				{
					test = m_particles[i].getPath().size() - 1;
					if (m_particles[i].getPath().at(test) == TARGET)
					{
						//done = true;
						pathfound = true;
					}

					if (test > m_particles[i].getpBest())
					{
						m_particles[i].setpBest(test);
						m_particles[i].setPBData();
					}
				}

			} // i

			/***************** GBEST CODE *****************
			gBestTest = minimum(adjMat);
			
			//If any particle's pBest value is better than the gBest value,
			//make it the new gBest Value.
			if (m_particles[gBestTest].getCost() > 0)
			{
				if (m_particles[gBestTest].getCost() < gBest)
				{
					gBest = gBestTest;
					bestPath = m_particles[gBestTest].getPath();
				}
			}

			if (gBest < MAX_INPUTS)
			{
				// Display best Particle
				std::cout << "Best Particle: " << gBest << std::endl;

				std::cout << bestPath[0] << ",";

				for (unsigned int j = 0; j < bestPath.size() - 1; j++)
				{
					std::cout << bestPath[j] << ",";
				} // j

				std::cout << bestPath[bestPath.size() - 1] << std::endl;
			}
			***************** GBEST CODE *****************/

			for (int i = 0; i < MAX_PARTICLES; i++)
			{
				nBest = getNBest(i);
				bestData = m_particles[nBest].getPBData();

				// Clear Particles
				m_particles[i].clearPath();

				getVelocity(nBest, adjMat);

				updateParticles(nBest, adjMat);
			}

			epoch += 1;
		}
		else
		{
			done = true;
		}

	} while (!done);

	/*****DEBUG MESSAGE*****
	int tempTotal = 0;
	std::cout << "Particle " << gBest << ": ";

	for (int z = 0; z < bestPath.size()-1; ++z)
	{
		std::cout << bestPath(z) << ", ";
	}

	std::cout << bestPath(bestPath.size()-1) << std::endl;

	for (int c = 0; c < bestPath.size()-1; ++c)
	{
		tempTotal += adjMat(bestPath[c])(bestPath[c+1]);
	}
	std::cout << "Cost: " << tempTotal << std::endl;
	*****DEBUG MESSAGE*****/
}


void Swarm::initParticles()
{
	m_particles.resize(MAX_PARTICLES);

	for (unsigned int i = 0; i < MAX_PARTICLES; ++i)
	{
		m_particles[i].init(MAX_INPUTS);
	}
}


void Swarm::initialize(std::vector<std::vector<int>> &adjMat)
{
	float temp = 0;

	phi = c1 + c2;
	/*** X = 2(|2-phi-sqrt(phi^2 - 4phi)|)^-1 ***/
	temp = phi * phi - 4.0f * phi;
	temp = std::abs(temp);
	temp = std::sqrt(temp);
	temp = (2.0f - phi - temp);
	temp = std::abs(temp);
	temp = 2.0f * std::pow(temp, -1.0f);
	X = temp;
	/*** X = 2(|2-phi-sqrt(phi^2 - 4phi)|)^-1 ***/

	MAX_INPUTS = adjMat.size();

	initParticles();	

	int total;

	for (int i = 0; i < MAX_PARTICLES; i++)
	{
		for (int j = 0; j < MAX_INPUTS; ++j)
		{
			m_particles[i].setVelocity(j, getRandomNumber(-10, 10));

			m_particles[i].setData(j, getRandomNumber(START_RANGE_MIN, START_RANGE_MAX));
		} // j

		total = 0;

		m_particles[i].setData(START, 1);

		total = testProblem(i, adjMat);

		if (m_particles[i].getPath().size() > 0)
		{
			m_particles[i].setpBest(total);
			m_particles[i].setPBData();
		}
	} // i

	/***************** GBEST CODE *****************
	for (int k = 0; k < MAX_PARTICLES; ++k)
	{
		if (m_particles[k].getCost() > 0)
		{
			gBest = k;
			bestPath = m_particles[k].getPath();
		}
	}
	***************** GBEST CODE *****************/
}


void Swarm::getVelocity(int gBestIndex, std::vector<std::vector<int>> &adjMat)
{
	/* from Kennedy & Eberhart(1995).
	vx[][] = vx[][] + 2 * rand() * (pbestx[][] - presentx[][]) +
	2 * rand() * (pbestx[][gbest] - presentx[][])
	*/

	int personalBest, globalBest, curPersonal;
	float vValue;

	for (int i = 0; i < MAX_PARTICLES; ++i)
	{
		for (int j = 0; j < MAX_INPUTS; ++j)
		{

			curPersonal = m_particles[i].getData(j);

			if (m_particles[i].getpBest() > 0)
			{
				personalBest = m_particles[i].getPBData()[j];
			}
			else personalBest = m_particles[i].getData(j);

			globalBest = bestData[j];

			// X * ( p[i].v + c1r1(pbest-p[i].x) + c2r2(gbest-p[i].x) )
			vValue = X * (m_particles[i].getVelocity(j) +
				c1 * gRand() * (personalBest - curPersonal) +
				c2 * gRand() * (globalBest - curPersonal)); 

			if (vValue > V_MAX)
			{
				m_particles[i].setVelocity(j, V_MAX);
			}
			else if (vValue < -V_MAX)
			{
				m_particles[i].setVelocity(j, -V_MAX);
			}
			else
			{
				m_particles[i].setVelocity(j, vValue);
			}
		}
	} // i
}


void Swarm::updateParticles(int gBestIndex, std::vector<std::vector<int>> &adjMat)
{
	int total, tempData;

	for (int i = 0; i < MAX_PARTICLES; i++)
	{
		for (int j = 0; j < MAX_INPUTS; j++)
		{
			tempData = m_particles[i].getData(j);
			m_particles[i].setData(j, tempData + static_cast<int>(m_particles[i].getVelocity(j)));
		} // j

		//Check pBest value.
		total = testProblem(i, adjMat);
		if (total > 0)
		{
			if (total < m_particles[i].getpBest())
			{
				m_particles[i].setpBest(total);
				m_particles[i].setPBData();
			}
		}
	} // i
}


int Swarm::testProblem(int index, std::vector<std::vector<int>> &adjMat)
{
	int total = 0;
	int cost = 0;
	int currNode = START;
	int nextNode = 0;
	int pathIndex = 0;
	int n = 0;

	bool done = false;
	bool loop = false;

	std::vector<int> costs;

	m_particles[index].setPath(currNode);
	m_particles[index].setData(currNode, N);
	pathIndex++;

	std::vector<int> nodes;

	do
	{
		cost = 0;
		nextNode = 0;
		loop = false;

		for (int i = 0; i < MAX_INPUTS; i++)
		{
			if (currNode == 279 || currNode == 298)
			{
				cost = 0;
			}
			// find connected nodes
			if (adjMat[currNode][i] != 0)
			{
				nodes.push_back(i);
			}

			// if all connected nodes have been found
			if (nodes.size() == 4)
			{
				break;
			}
		} // i

		// select connected node
		for (unsigned int j = 0; j < nodes.size(); ++j)
		{
			cost = m_particles[index].getData(nodes[j]) - m_particles[index].getData(currNode);

			if (cost > -M)				// Ni+1 - Ni > -M
			{
				costs.push_back(cost);
			}
			else
				costs.push_back(N);
		}

		if (costs.size() > 1)
		{
			// find node with the highest value
			for (unsigned int k = 0; k < costs.size(); ++k)
			{
				cost = costs[0];
				nextNode = nodes[0];
				if (costs[k] > cost)
				{
					cost = costs[k];
					nextNode = nodes[k];
				}
			}
			if (cost == 0)
			{
				n = MAX_INPUTS;
				loop = true;
			}
		}
		else if (costs.size() == 1)
		{
			if (costs[0] == 0)
			{
				loop = true;
			}

			nextNode = nodes[0];
		}
		else
		{
			m_particles[index].setCost(N);
			loop = true;
		}

		costs.clear();
		nodes.clear();

		if (loop == false)
		{
			currNode = nextNode;

			m_particles[index].setPath(currNode);
			m_particles[index].setData(currNode, N);

			nodesSearched++;
		}

		if (currNode == TARGET)
		{
			if (bestPath.empty() == false)
			{
				compareCosts(index, adjMat);
			}
			else
			{
				bestPath = m_particles[index].getPath();
			}

			done = true;
		}

		if (loop == true)
		{
			done = true;
		}

		n++;

	} while (done == false);

	// if path is valid
	if (m_particles[index].getPath().back() == TARGET)
	{
		// get the cost of the path (p = next Node)
		for (unsigned int p = 0; p < m_particles[index].getPath().size()-1; ++p)
		{
			// cost = the cost of to travel to the next node
			cost = adjMat[m_particles[index].getPath()[p]][m_particles[index].getPath()[p + 1]];
			total += cost;
		}
	}
	else
	{
		total = 0;
	}

	m_particles[index].setCost(total);

	return total;
}


float Swarm::gRand()
{
	// Returns a pseudo-random float between 0.0 and 1.0
	return float(rand() / (RAND_MAX + 1.0));
}


int Swarm::getRandomNumber(int low, int high)
{
	// Returns a pseudo-random integer between low and high.
	return low + int(((high - low) + 1) * rand() / (RAND_MAX + 1.0));
}


int Swarm::minimum(std::vector<std::vector<int>> &adjMat)
{
	//Returns an array index.
	int winner = 0;
	bool foundNewWinner;
	bool done = false;

	int cCost = 0, wCost = 0;

	// Winner needs to start of with a valid value
	for (unsigned int j = 0; j < MAX_PARTICLES; j++)
	{
		if (m_particles[winner].getCost() > 0)
		{
			winner = j;
			break;
		}
	}

	do
	{
		foundNewWinner = false;
		for (unsigned int i = 0; i < MAX_PARTICLES; i++)
		{
			if (i != winner)             //Avoid self-comparison.
			{
				if (m_particles[i].getCost() > 0)
				{
					cCost = m_particles[i].getCost();
					wCost = m_particles[winner].getpBest();
					// The minimum has to be in relation to the Target.
					if (cCost < wCost)
					{
						winner = i;
						foundNewWinner = true;
						bestPath = m_particles[i].getPath();
					}
				}
			}
		} // i

		if (foundNewWinner == false)
		{
			done = true;
		}

	} while (!done);

	return winner;
}

int Swarm::getNBest(int &index)
{
	int cost;
	std::vector <int> indexes;
	int winner = MAX_PARTICLES;
	int n = index;

	if (index == 0)
	{
		if (m_particles[MAX_PARTICLES - 1].getpBest() > 0)
		{
			indexes.push_back(MAX_PARTICLES - 1);
		}

		if (m_particles[index].getpBest() > 0)
		{
			indexes.push_back(index);
		}

		if (m_particles[index + 1].getpBest() > 0)
		{
			indexes.push_back(index + 1);
		}
	}
	else if (index == MAX_PARTICLES - 1)
	{
		if (m_particles[index - 1].getpBest() > 0)
		{
			indexes.push_back(index - 1);
		}

		if (m_particles[index].getpBest() > 0)
		{
			indexes.push_back(index);
		}

		if (m_particles[0].getpBest() > 0)
		{
			indexes.push_back(0);
		}
	}
	else
	{
		if (m_particles[index - 1].getpBest() > 0)
		{
			indexes.push_back(index - 1);
		}

		if (m_particles[index].getpBest() > 0)
		{
			indexes.push_back(index);
		}

		if (m_particles[index + 1].getpBest() > 0)
		{
			indexes.push_back(index + 1);
		}
	}

	// If no neighbours have a path
	// select the neighbour with the longest path
	if (indexes.empty() == true)
	{
		unsigned int length = m_particles[index].getPBData().size();

		winner = index;

		if (index == 0)
		{
			if (length < m_particles[MAX_PARTICLES - 1].getPath().size())
			{
				length = m_particles[MAX_PARTICLES - 1].getPath().size();
				winner = MAX_PARTICLES - 1;
			}

			if (length < m_particles[index + 1].getPath().size())
			{
				length = m_particles[index + 1].getPath().size();
				winner = index + 1;
			}
		}
		else if (index == MAX_PARTICLES - 1)
		{
			if (length < m_particles[index - 1].getPath().size())
			{
				length = m_particles[index - 1].getPath().size();
				winner = index - 1;
			}

			if (length < m_particles[0].getPath().size())
			{
				length = m_particles[0].getPath().size();
				winner = 0;
			}
		}
		else
		{
			if (length < m_particles[index - 1].getPath().size())
			{
				length = m_particles[index - 1].getPath().size();
				winner = index - 1;
			}

			if (length < m_particles[index + 1].getPath().size())
			{
				length = m_particles[index + 1].getPath().size();
				winner = index + 1;
			}
		}
	}
	else
	{
			cost = m_particles[indexes[0]].getpBest();
			winner = indexes[0];

			for (unsigned int i = 1; i < indexes.size(); ++i)
			{
				if (m_particles[indexes[i]].getpBest() < cost)
				{
					cost = m_particles[indexes[i]].getpBest();
					winner = indexes[i];
				}
			}
	}

	return winner;
}


void Swarm::setInputs(int &inputs)
{
	MAX_INPUTS = inputs;
}


void Swarm::compareCosts(int &index, std::vector<std::vector<int>> &adjMat)
{
	int cost, total = 0, bestTotal = 0;
	// get the cost of the path (p = next Node)
	for (unsigned int p = 0; p < m_particles[index].getPath().size() - 1; ++p)
	{
		// cost = the cost of to travel to the next node
		cost = adjMat[m_particles[index].getPath()[p]][m_particles[index].getPath()[p + 1]];
		total += cost;
	}

	for (unsigned int i = 0; i < bestPath.size() - 1; ++i)
	{
		// cost = the cost of to travel to the next node
		cost = adjMat[bestPath[i]][bestPath[i + 1]];
		bestTotal += cost;
	}

	if (total < bestTotal)
	{
		bestPath = m_particles[index].getPath();
	}
}


void Swarm::setPath(std::vector<int> &path)
{
	this->bestPath = path;
}


std::vector<int> Swarm::getPath()
{
	return bestPath;
}