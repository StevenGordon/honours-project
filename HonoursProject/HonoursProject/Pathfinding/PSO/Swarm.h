#pragma once

#include <iostream>
#include <iomanip>
#include <cctype>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <algorithm>
#include <SDL.h>

#include "Particle.h"

/*
Sources:
Kennedy, J.and Eberhart, R.C.Particle swarm optimization.
Proc.IEEE int'l conf. on neural networks Vol. IV, pp. 1942-1948.
IEEE service center, Piscataway, NJ, 1995.
PSO Tutorial found at : http ://www.swarmintelligence.org/tutorials.php
*/
class Swarm
{
public:
	//Swarm();
	~Swarm();

	bool findPath(int startNode, int goalNode, std::vector<std::vector<int>> &adjMat);

	void psoAlgorithm(std::vector<std::vector<int>> &adjMat);

	void getVelocity(int gBestIndex, std::vector<std::vector<int>> &adjMat);

	void updateParticles(int gBestIndex, std::vector<std::vector<int>> &adjMat);

	void initParticles();

	void initialize(std::vector<std::vector<int>> &adjMat);

	int testProblem(int index, std::vector<std::vector<int>> &adjMat);

	float gRand();

	int getRandomNumber(int low, int high);

	int minimum(std::vector<std::vector<int>> &adjMat);

	int getNBest(int &index);

	void setInputs(int &inputs);

	void compareCosts(int &index, std::vector<std::vector<int>> &adjMat);

	void setPath(std::vector<int> &path);
	std::vector<int> getPath();

	int getMaxInputs() { return MAX_INPUTS; }

private:
	int TARGET;
	int START;
	static const int MAX_PARTICLES = 10;
	int MAX_INPUTS;							// Number of variables to be optimized /*This is the path (inputs = the number of nodes)*/
	const float V_MAX = 5000.0;				// Maximum velocity change allowed.
	const int MAX_EPOCHS = 500;

	float c1 = 2.05f;	// Learning factor 1
	float c2 = 2.05f;	// Learning factor 2
	float phi;

	float X = 0.0f;		// Constriction Factor

	int N = -5000;

	//The particles will be initialized with data randomly chosen within the range
	//of these starting min and max values: 
	const int START_RANGE_MIN = -1500;
	const int START_RANGE_MAX = 1500;

	const int M = 4;	// The nextNode - currentNode > -M

	bool pathfound = false;

	int nBest = 0;

	std::vector<int> bestData;

	std::vector<int> bestPath;

	std::vector<Particle> m_particles;

	int nodesSearched = 0;

	// Timer functions
	int StartTimer;
	int endTimer;
	int elapsedtime;
};