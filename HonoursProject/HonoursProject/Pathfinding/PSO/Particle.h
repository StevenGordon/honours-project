#pragma once

#include <vector>

class Particle
{
public:
	Particle();
	~Particle();

	void init(int maxInputs);

	int getData(int index) const;
	void setData(int index, int value);

	int getpBest() const;
	void setpBest(int value);

	float getVelocity(int index) const;
	void setVelocity(int index, float value);

	std::vector<int> getPath() const;
	void setPath(int value);
	void clearPath();

	std::vector<int> getPBData() const;
	// Takes no input as it already contains the input (pPath)
	void setPBData();

	int getCost() const;
	void setCost(int value);

private:
	int m_maxInputs;
	
	std::vector<int> mData;			// Vector of positions

	std::vector<int> pPath;			// partial path

	int pCost;						// Cost of current pPath

	std::vector<int> pbData;		// personal best Path

	int mpBest;						// personal best path cost

	std::vector<float> mVelocity;	// Particle velocity
};