#pragma once

#include "Map\Node.h"
#include "Map\Grid.h"
#include "Astar\Astar.h"
#include "PSO\Swarm.h"
#include "ACO\Colony.h"

class Pathfinding
{
public:
	Pathfinding();
	~Pathfinding();

	bool findPath(Node &start, Node &goal, int type);

	std::vector<std::pair<int, int>> getPath(int type);

	void setGrid(std::vector<std::vector<int>> map);

	Grid &getGrid() { return m_grid; }

private:
	Grid m_grid;

	Astar m_aStar;

	Swarm *m_pso;

	Colony m_aco;

	// This is the size of the grid
	int m_sizeX;
};

