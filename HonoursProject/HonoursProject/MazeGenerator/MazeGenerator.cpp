#include "MazeGenerator.h"


MazeGenerator::MazeGenerator(const int imaxSizeX, const int imaxSizeY)
	: m_extraConnectorChance(0), m_roomExtraSize(0), m_windingPercent(0), m_maxSizeX(imaxSizeX), m_maxSizeY(imaxSizeY)
{
	m_directions[0] = std::pair<int, int>{ 0, -1 };	// Down
	m_directions[1] = std::pair<int, int>{ 0, 1 };	// Up
	m_directions[2] = std::pair<int, int>{ -1, 0 };	// Left
	m_directions[3] = std::pair<int, int>{ 1, 0 };	// Right

	// TODO: Give default values for m_defaultSeeds

	std::cout << "MazeGenerator Constructor" << std::endl;
}


MazeGenerator::~MazeGenerator()
{
	unsigned int i = 0;

	if (m_rooms.empty() == false)
	{
		for (unsigned int i = 0; i < m_rooms.size(); ++i)
		{
			m_rooms.at(i).~Room();
		}

		m_rooms.clear();
	}

	if (m_region.empty() == false)
	{
		for (i = 0; i < m_region.size(); ++i)
		{
			m_region.at(i).clear();
			m_region.at(i).resize(0);
		}

		m_region.clear();
		m_region.resize(0);
	}
	
	std::cout << "MazeGenerator Deconstructor" << std::endl;
}


bool MazeGenerator::generateMaze(int noRooms, int isizeX, int isizeY, int roomConnectionChance, int seed)
{
	/// The random dungeon generator.
	///
	/// Starting with a stage of solid walls, it works like so:
	///
	/// 1. Place a number of randomly sized and positioned rooms. If a room
	///    overlaps an existing room, it is discarded. Any remaining rooms are
	///    carved out.
	/// 2. Any remaining solid areas are filled in with mazes. The maze generator
	///    will grow and fill in even odd-shaped areas, but will not touch any
	///    rooms.
	/// 3. The result of the previous two steps is a series of unconnected rooms
	///    and mazes. We walk the stage and find every tile that can be a
	///    "connector". This is a solid tile that is adjacent to two unconnected
	///    regions.
	/// 4. We randomly choose connectors and open them or place a door there until
	///    all of the unconnected regions have been joined. There is also a slight
	///    chance to carve a connector between two already-joined regions, so that
	///    the dungeon isn't single connected.
	/// 5. The mazes will have a lot of dead ends. Finally, we remove those by
	///    repeatedly filling in any open tile that's closed on three sides. When
	///    this is done, every corridor in a maze actually leads somewhere.
	///
	/// The end result of this is a multiply-connected dungeon with rooms and lots
	/// of winding corridors.

	bool error = false;

	std::default_random_engine generator(seed);

	m_extraConnectorChance = roomConnectionChance;
	// Value that effects the shape of the rooms
	// Used to calculate rectangularity
	m_roomExtraSize = 0;
	m_windingPercent = 30; // likleu hood of the corridor creation changing direction

	m_sizeX = isizeX;
	m_sizeY = isizeY;

	if (m_sizeX > m_maxSizeX)
	{
		m_sizeX = m_maxSizeX;
		std::cout << "x size greater than max!" << std::endl;
	}

	if (m_sizeY > m_maxSizeY)
	{
		m_sizeY = m_maxSizeY;
		std::cout << "y size greater than max!" << std::endl;
	}

	// Set m_region size
	m_region.resize( (m_sizeX/10), std::vector<int>((m_sizeY/10), 0) );

	addRooms(noRooms, generator);
	
	for (int y = 1; y < ((m_sizeY / 10) - 1); y += 2)
	{
		for (int x = 1; x < ((m_sizeX/10) - 1); x += 2)
		{
			m_currentRegion = { x, y };

			if ( m_region.at(m_currentRegion.first).at(m_currentRegion.second) != tile::empty )
			{
				continue;
			}

			growMaze(m_currentRegion, generator);
		}
	}
	
	connectRegions(generator);
	//_removeDeadEnds();

	return error;
}


/// Places rooms ignoring the existing maze corridors.
void MazeGenerator::addRooms(int noRooms, std::default_random_engine &generator)
{
	Room room;

	m_noRooms = noRooms;

	m_roomID = 0;
	int unsigned maxSize = ((m_sizeX / 10) + (m_sizeY / 10)) / 2;
	int unsigned rectangularity = 0;
	int unsigned x, y, width, height = 0;

	bool overlaps = false;

	// Range for the RNG current values are tests
	std::uniform_int_distribution<int> roomSize_RNG(1, 3 + m_roomExtraSize);
		
	// Room size
	int unsigned size = 0;

	while ( m_rooms.size() != noRooms )
	{
		overlaps = false;

		size =  (roomSize_RNG(generator) * 2 + 1);

		std::uniform_int_distribution<int> rectangularity_RNG(0, 1 + size / 2);

		width = size;
		height = size;

		rectangularity = rectangularity_RNG(generator) * 2;

		if (rectangularity % 2 == 0)
		{
			width += rectangularity;
		}
		else
		{
			height += rectangularity;
		}

		std::uniform_int_distribution<int> roomPlacementX_RNG(1, (m_sizeX/10 - width - 1) );
		std::uniform_int_distribution<int> roomPlacementY_RNG(1, (m_sizeY/10 - height - 1) );

		x = 3 + ( (roomPlacementX_RNG(generator) / 2) * 2 ) - 2;
		y = 3 + ( (roomPlacementY_RNG(generator) / 2) * 2 ) - 2;

		room.init(x, y, width, height, m_roomID, tile::room);

		if (m_rooms.size() == 0)
		{
			m_rooms.push_back(room);
			++m_roomID;
		}
		else
		{
			for (int j = 0; j < m_roomID; ++j)
			{
				if (room.overlap(m_rooms.at(j)) == true && room.distanceTo(m_rooms.at(j)) <= 20)
				{
					overlaps = true;
					break;
				}
			}

			if (overlaps == true)
			{
				// Skips the rest of this instance of the loop
				// and does not execute and code in the loop below this
				continue;
			}

			m_rooms.push_back(room);
			++m_roomID;
		}
	}

	// Update m_region
	for (int i = 0; i < m_noRooms; ++i)
	{
		carve(m_rooms.at(i), tile::room);
	}

}


void MazeGenerator::carve(Room &room, int type)
{
	int tileType = type;

	int x = room.getPos().first;
	int y = room.getPos().second;

	int endX = (room.getPos().first + room.getSize().first);
	int endY = (room.getPos().second + room.getSize().second);

	for (x; x < endX; ++x)
	{
		for (y; y < endY; ++y)
		{
			m_region.at(x).at(y) = tileType;
		}
		y = room.getPos().second;
	}
}


/// Implementation of the "growing tree" algorithm from here:
/// http://www.astrolog.org/labyrnth/algrithm.htm.
/*
*	Growing tree algorithm: This is a general algorithm, capable of creating Mazes of different textures.
*	It requires storage up to the size of the Maze. Each time you carve a cell, add that cell to a list.
*	Proceed by picking a cell from the list, and carving into an unmade cell next to it.
*	If there are no unmade cells next to the current cell, remove the current cell from the list.
*	The Maze is done when the list becomes empty.
*	The interesting part that allows many possible textures is how you pick a cell from the list.
*	For example, if you always pick the most recent cell added to it, this algorithm turns into the recursive backtracker.
*	If you always pick cells at random, this will behave similarly but not exactly to Prim's algorithm.
*	If you always pick the oldest cells added to the list, this will create Mazes with about as low a "river" factor as possible,
*	even lower than Prim's algorithm. If you usually pick the most recent cell, but occasionally pick a random cell,
*	the Maze will have a high "river" factor but a short direct solution.
*	If you randomly pick among the most recent cells, the Maze will have a low "river" factor but a long windy solution.
*/
void MazeGenerator::growMaze(std::pair<int, int> start, std::default_random_engine &generator)
{
	std::uniform_int_distribution<int> rndWinding(0, 100);

	std::stack< std::pair<int,int> > regions;
	std::vector<int> unmadeRegions;

	int lastDir = 5; // Not a direction to make the first direction random
	int direction;

	Room corridor = Room();

	regions.push({ start.second, start.second });

	corridor.init(regions.top().first, regions.top().second, 1, 1, m_roomID, tile::corridor);
	carve(corridor, tile::corridor);
	m_rooms.push_back(corridor);
	++m_roomID;
	
	m_region.at(m_currentRegion.first).at(m_currentRegion.second) = tile::corridor;
	
	while (regions.empty() == false)
	{
		m_currentRegion = regions.top();

		for (int i = 0; i < 4; ++i)
		{
			if (canCarve(m_currentRegion, m_directions[i]))
			{
				std::pair<int, int> dirDoubled = { m_directions[i].first * 2, m_directions[i].second * 2 };
				if (canCarve(m_currentRegion, dirDoubled) == true)
				{
					unmadeRegions.push_back(i);
				}
			}
		}

		if (unmadeRegions.empty() == false)
		{
			bool contains = false;
			for (unsigned int i = 0; i < unmadeRegions.size(); ++i)
			{
				if (unmadeRegions.at(i) == lastDir)
				{
					contains = true;
					break;
				}
			}

			/// Based on how "windy" passages are, try to prefer carving in the
			/// same direction.
			if (contains && rndWinding(generator) > m_windingPercent)
			{
				direction = lastDir;
			}
			else
			{
				std::uniform_int_distribution<int> rndRegion(0, unmadeRegions.size() - 1);
				direction = unmadeRegions[rndRegion(generator)];
			}

			// Add first corridor
			m_currentRegion.first += m_directions[direction].first;
			m_currentRegion.second += m_directions[direction].second;

			corridor.init(m_currentRegion.first, m_currentRegion.second, 1, 1, m_roomID, tile::corridor);
			carve(corridor, tile::corridor);
			m_rooms.push_back(corridor);
			++m_roomID;

			m_region.at(m_currentRegion.first).at(m_currentRegion.second) = tile::corridor;

			// Add second corridor
			m_currentRegion.first += m_directions[direction].first;
			m_currentRegion.second += m_directions[direction].second;

			corridor.init(m_currentRegion.first, m_currentRegion.second, 1, 1, m_roomID, tile::corridor);
			carve(corridor, tile::corridor);
			m_rooms.push_back(corridor);
			++m_roomID;

			m_region.at(m_currentRegion.first).at(m_currentRegion.second) = tile::corridor;

			// Add second corridor to list
			regions.push( { corridor.getPos().first, corridor.getPos().second } );

			// Set last direction
			lastDir = direction;

			// Clear list of possible directions
			unmadeRegions.clear();
		}
		else
		{
			/// No adjacent uncarved cells.
			regions.pop();
			/// This path has ended.
			lastDir = NULL;
		}
	}
}


bool MazeGenerator::inbounds(std::pair<int, int> &pos, std::pair<int, int> &direction)
{
	// Inside the bounds
	// if (posX + dir is less than 1) OR (posX + dir is greater than (mazeX + mazeWidth)) => outside of bounds 
	if ((pos.first + direction.first < 1) || (pos.first + direction.first >(m_sizeX / 10) - 1))
	{
		return false;
	}
	else
	{
		// if (posY + dir is less than 1) OR (posY + dir is greater than (mazeY + mazeHeight)) => outside of bounds 
		if ((pos.second + direction.second < 1) || (pos.second + direction.second >(m_sizeY / 10) - 1))
		{
			return false;
		}

		return true;
	}
}


/// Gets whether or not an opening can be carved from the given starting
/// [Cell] at [pos] to the adjacent Cell facing [direction]. Returns `true`
/// if the starting Cell is in bounds and the destination Cell is filled
/// (or out of bounds).</returns>
bool MazeGenerator::canCarve(std::pair<int,int> &pos, std::pair<int,int> &direction)
{
	if (inbounds(pos, direction) == true)
	{
		// In a suitable area
		if (m_region.at((pos.first + direction.first)).at((pos.second + direction.second)) != tile::empty)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	return false;
}



void MazeGenerator::connectRegions(std::default_random_engine &generator)
{
	/// Find all of the tiles that can connect two (or more) regions.

	// Vector of a list of pairs
	// connectorRegions[room][possibleConnector{Posx,Posy}]
	std::vector< std::vector<std::pair<int, int>> > connectorRegions(m_noRooms);
	std::vector<std::pair<int, int>> regions;

	std::pair<int, int> pos;

	Room door = Room();

	int x, y, temp, width, height, loop = 0;

	int i = 0;

	int noConnections = 0;

	std::uniform_int_distribution<int> Connections(1, 4);

	for (int n = 0; n < m_noRooms; ++n)					// For all rooms
	{
		x = m_rooms[n].getPos().first;					// Left side of room
		width = m_rooms[n].getSize().first;				// Right side of room
		y = m_rooms[n].getPos().second;					// Bottom side of room
		height = m_rooms[n].getSize().second;			// Top side of room
		
		for (temp = 0; temp < height; ++temp)		// Left
		{
			pos = { (x - 1), (y + temp) };

			if (addJunction(pos, dir::left) == true)
			{
				connectorRegions[n].push_back(pos);
			}
		}

		for (temp = 0; temp < width; ++temp)		// Bottom
		{
			pos = { (x + temp), (y - 1) };

			if (addJunction(pos, dir::down) == true)
			{
				connectorRegions[n].push_back(pos);
			}
		}

		for (temp = 0; temp < height; ++temp)		// Right 
		{
			pos = { (x+width), (y + temp) };

			if (addJunction(pos, dir::right) == true)
			{
				connectorRegions[n].push_back(pos);
			}
		}

		for (temp = 0; temp < width; ++temp)		// Top
		{
			pos = { (x + temp), (y+height) };

			if (addJunction(pos, dir::up) == true)
			{
				connectorRegions[n].push_back(pos);
			}
		}

	}

	int rndConnection = 0;

	unsigned int dirs, roomCons = 0;
	bool flag = false;

	// For all rooms
	for (i = 0; i < m_noRooms; ++i)
	{
		noConnections = Connections(generator);		// Number of connections for the room
		
		for (int j = 0; j < noConnections; ++j)
		{
			flag = false;

			while (flag == true)
			{
				flag = false;
				std::uniform_int_distribution<int> connectPos(0, (connectorRegions[i].size() - 1));	// needed here as size changes each loop

				rndConnection = connectPos(generator);

				pos = connectorRegions[i][rndConnection];

				if (m_region[pos.first][pos.second] == tile::connector)	// is this tile already a connector
				{
					flag = true;
					// Remove connector
					connectorRegions[i][rndConnection].swap(connectorRegions[i].back());
					connectorRegions[i].pop_back();

					// Remove adjacent connectors
					for (dirs = 0; dirs < 4; ++dirs)
					{
						for (roomCons = 0; roomCons < connectorRegions[i].size(); ++roomCons)
						{
							if (inbounds(pos, m_directions[dirs]) == true)
							{
								pos = { door.getPos().first + m_directions[dirs].first, door.getPos().second + m_directions[dirs].second };
								if (pos == connectorRegions[i][roomCons])
								{
									connectorRegions[i][roomCons].swap(connectorRegions[i].back());
									connectorRegions[i].pop_back();
								}
							}
						}
					}
				}
				else
				{
					for (dirs = 0; dirs < 4; ++dirs)			// is an adjacent tile a connector
					{
						pos = connectorRegions[i][rndConnection];
						if (m_region[pos.first][pos.second] != empty)
						{
							flag = true;
							// Remove connector
							connectorRegions[i][rndConnection].swap(connectorRegions[i].back());
							connectorRegions[i].pop_back();
						}
					}
				}
			}
			
			// Add connector
			door.init(connectorRegions[i][rndConnection].first, connectorRegions[i][rndConnection].second, 1, 1, m_rooms.size(), tile::connector);
			carve(door, tile::connector);
			m_rooms.push_back(door);

			// Remove connector from list
			connectorRegions[i][rndConnection].swap(connectorRegions[i].back());
			connectorRegions[i].pop_back();

			// Remove adjacent connectors form list
			for (dirs = 0; dirs < 4; ++dirs)
			{
				for (roomCons = 0; roomCons < connectorRegions[i].size(); ++roomCons)
				{
					pos = { door.getPos().first + m_directions[dirs].first, door.getPos().second + m_directions[dirs].second };
					if (pos == connectorRegions[i][roomCons])
					{
						connectorRegions[i][roomCons].swap(connectorRegions[i].back());
						connectorRegions[i].pop_back();
					}
				}
			}
		}
	}

}

bool MazeGenerator::addJunction(std::pair<int,int> &pos, int dir)
{
	// This function checks is a tile and its neighbour (depending on side)
	// is in the bounds and the neighbour is not empty
	// the neigbour is the tile + dir
	// if tile is on the left neighbour = (tile + left),
	// if tile is on the bottom neighbour = (tile + down)...etc
	std::pair<int,int> temp;

	if (inbounds(pos, std::pair < int, int > {0, 0}) == true)			// Is this tile inbounds
	{
		temp = { (pos.first + m_directions[dir].first), (pos.second + m_directions[dir].second) };

		if (inbounds(pos, m_directions[dir]) == true)					// Is this tile + direction inbounds
		{
			if (m_region.at(temp.first).at(temp.second) != tile::empty)	// Is this tile a room or corridor
			{
				return true;
			}
		}
	}

	return false;
}

	/*
	void _removeDeadEnds()
	{
		var done = false;

		while (!done)
		{
			done = true;

			for (var pos in bounds.inflate(-1))
			{
				if (getTile(pos) == Tiles.wall) continue;

				// If it only has one exit, it's a dead end.
				var exits = 0;
				for (var dir in Direction.CARDINAL) {
					if (getTile(pos + dir) != Tiles.wall) exits++;
				}

				if (exits != 1) continue;

				done = false;
				setTile(pos, Tiles.wall);
	*/