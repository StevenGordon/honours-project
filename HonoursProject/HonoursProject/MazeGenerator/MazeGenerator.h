#pragma once

/*	
	--------------B00228322--------------
	Any comments with /// are not my own
	All Comments with // are my own
*/

#include "../Renderables/Room.h"
#include "../Pathfinding/Map/Grid.h"

#include <iostream>
#include <vector>
#include <stack>
#include <random>

class MazeGenerator
{
public:
	MazeGenerator(const int imaxSizeX, const int imaxSizeY);

	~MazeGenerator();

	bool generateMaze(int noRooms, int isizeX, int isizeY, int roomConnectionChance, int seed);

	void addRooms(int noRooms, std::default_random_engine &generator);

	// Adds room to m_region
	void carve(Room &room, int type);

	void growMaze(std::pair<int, int> start, std::default_random_engine &generator);

	bool inbounds(std::pair<int, int> &pos, std::pair<int, int> &direction);

	bool canCarve(std::pair<int, int> &pos, std::pair<int, int> &direction);

	void connectRegions(std::default_random_engine &generator);

	bool addJunction(std::pair<int, int> &pos, int dir);

	std::vector<Room> &getRooms() { return m_rooms; }

	int getNoMazeRooms() { return m_rooms.size(); }

	int getNoRooms() { return m_noRooms; }

	std::vector<std::vector<int>> getRegions() { return m_region; }

	enum tile { empty, corridor, room, connector };

	enum dir { down, up, left, right };

private:
	const int m_maxSizeX;
	const int m_maxSizeY;

	int m_sizeX;
	int m_sizeY;

	int m_noRooms;

	int m_currentSeed;
	int m_defaultSeeds[9];

	/// The inverse chance of adding a connector between two regions that have
	/// already been joined. Increasing this leads to more loosely connected
	/// dungeons.
	int m_extraConnectorChance;

	/// Increasing this allows rooms to be larger.
	int m_roomExtraSize;

	int m_windingPercent;

	// m_rooms indlude both rooms, corridors and doors
	std::vector<Room> m_rooms;

	int m_roomID;

	std::pair<int, int> m_directions[4];

	// 0 = empty, 1 = corridor, 2 = room, 3 = door
	std::vector<std::vector<int>> m_region;

	// Current Regions address
	std::pair<int,int> m_currentRegion;

};

