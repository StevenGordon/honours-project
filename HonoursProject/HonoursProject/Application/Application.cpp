#include "Application.h"

Application::Application()
{
	backgroundColour[0] = 0x46;
	backgroundColour[1] = 0x50;
	backgroundColour[2] = 0x8b;
	backgroundColour[3] = 0xFF; // Not Transparent

	maze = new MazeGenerator(Screen_Witdh, Screen_Height);

	std::cout << "Application Constructed" << std::endl;
}


Application::~Application()
{
	// Destroy renderer
	SDL_DestroyRenderer(renderer);
	renderer = NULL;

	// Destroy window
	SDL_DestroyWindow(window);
	window = NULL;

	// Quit SDL
	SDL_Quit();

	delete maze;
	maze = nullptr;

	std::cout << "Application Destructed" << std::endl;
}


bool Application::initSDL()
{

	bool flag = true;

	// Initialisation check
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "SDL could not inistialise! SDL_Error: " << SDL_GetError() << std::endl;

		flag = false;
	}
	else
	{
		// Create window
		window = SDL_CreateWindow("SWARM INT. PATHFINDING", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, Screen_Witdh, Screen_Height, SDL_WINDOW_SHOWN);

		if (window == NULL)
		{
			std::cout << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
			flag = false;
		}
		else
		{
			renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

			if (renderer == NULL)
			{
				std::cout << "Renderer not created! SDL Error: " << SDL_GetError() << std::endl;
				flag = false;
			}
			else
			{
				SDL_SetRenderDrawColor(renderer, backgroundColour[0], backgroundColour[1], backgroundColour[2], backgroundColour[3]);
			}
		}
	}

	return flag;
}


bool Application::initMaze(int noRooms, int isizeX, int isizeY, int roomConnectionChance, int seed)
{
	bool flag = false;

	flag = maze->generateMaze(noRooms, isizeX, isizeY, roomConnectionChance, seed);

	m_pathfinding.setGrid(maze->getRegions());

	m_rooms = maze->getRooms();

	return flag;
}


void Application::update()
{
	quit = false;

	while (quit == false)
	{
		
		while (SDL_PollEvent(&event) != 0)
		{
			
			if (event.type == SDL_QUIT)
			{
				quit = true;
			}
			else if (event.type == SDL_KEYDOWN)
			{
				quit = keyboard(event, quit);
			}

			// Clear screen
			SDL_SetRenderDrawColor(renderer, backgroundColour[0], backgroundColour[1], backgroundColour[2], backgroundColour[3]);
			SDL_RenderClear(renderer);

			for (int i = 0; i < maze->getNoMazeRooms(); ++i)
			{
				m_rooms.at(i).render(renderer);
			}

			// Update screen
			SDL_RenderPresent(renderer);
		}

	}
}


bool Application::keyboard(SDL_Event ievent, bool iquit)
{
	int lastNode = m_pathfinding.getGrid().getGrid().size()-1;

	// TODO: Replace input test with correct code
	// Input Test
	switch (ievent.key.keysym.sym)
	{
	case SDLK_1:
		if (m_pathfinding.findPath(m_pathfinding.getGrid().getGrid().at(0), m_pathfinding.getGrid().getGrid().at(19), 1) == true)
		{
			int size = m_pathfinding.getPath(1).size();

			m_path.resize(size);

			m_path = m_pathfinding.getPath(1);

			std::cout << "A* Path Found!" << std::endl;

			if (m_path.empty() == false)
			{
				recolour(2);
			}
		}
		else std::cout << "A* Path not found!" << std::endl;

		break;

	case SDLK_2:
		if (m_pathfinding.findPath(m_pathfinding.getGrid().getGrid().at(0), m_pathfinding.getGrid().getGrid().at(19), 2) == true)
		{
			int size = m_pathfinding.getPath(2).size();

			m_path.resize(size);

			m_path = m_pathfinding.getPath(2);

			std::cout << "ACO Path Found!" << std::endl;

			if (m_path.empty() == false)
			{
				recolour(3);
			}
		}
		else std::cout << "ACO Path not found!" << std::endl;

		break;

	case SDLK_3:
		if (m_pathfinding.findPath(m_pathfinding.getGrid().getGrid().at(0), m_pathfinding.getGrid().getGrid().at(19), 3) == true)
		{
			int size = m_pathfinding.getPath(3).size();

			m_path.resize(size);

			m_path = m_pathfinding.getPath(3);

			std::cout << "PSO Path Found!" << std::endl;

			if (m_path.empty() == false)
			{
				recolour(4);
			}
		}
		else std::cout << "PSO Path not found!" << std::endl;

		break;

	case SDLK_ESCAPE:
		iquit = true;

		break;

	default:
		std::cout << "a key has been pressed" << std::endl;

		break;
	}

	return iquit;
}


void Application::recolour(int colour)
{
	for (unsigned int i = 0; i < m_rooms.size(); ++i)
	{
		for (unsigned int j = 0; j < m_path.size(); ++j)
		{
			if (m_path[j] == m_rooms[i].getPos())
			{
				m_rooms[i].recolour(colour);
			}
		}
	}
}