#pragma once

#include "../MazeGenerator/MazeGenerator.h"
#include "../Pathfinding/Pathfinding.h"

#include <utility>
#include <SDL.h>
#include <iostream>

const int Screen_Witdh = 610;
const int Screen_Height = 610;

class Application
{
public:
	Application();
	~Application();

	bool initSDL();
	bool initMaze(int noRooms, int isizeX, int isizeY, int roomConnectionChance, int seed);

	void update();

	bool keyboard(SDL_Event ievent, bool iquit);

	void recolour(int colour);

private:
	MazeGenerator *maze;

	SDL_Window *window;
	
	SDL_Renderer *renderer;

	SDL_Event event;

	Pathfinding m_pathfinding;

	// Renderer Colour (HEX values => 0x00 = 0, 0xFF = 255)
	Uint8 backgroundColour[4];

	std::vector<Room> m_rooms;

	std::vector<std::pair<int, int>> m_path;

	bool quit;
};
