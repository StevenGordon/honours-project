#pragma once

#include<utility>

class Bounds
{
public:
	Bounds() {}
	Bounds(int x, int y, int width, int height);
	~Bounds();

	// May need to resize due to removals of deadends
	void resize(int x, int y, int width, int height);

	// Is a point inside this bounds
	bool contains(std::pair<int,int> &pos);

	// Is another bounds overlapping with this bounds
	bool overlap(Bounds &other);

	int getX() { return m_x; }
	int getY() { return m_y; }
	int getWidth() { return m_w; }
	int getHeight() { return m_h; }

private:
	int m_x;
	int m_y;
	int m_w;
	int m_h;
};

