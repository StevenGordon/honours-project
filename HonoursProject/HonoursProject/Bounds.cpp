#include "Bounds.h"


Bounds::Bounds(int x, int y, int width, int height) : m_x(x), m_y(y), m_w(width), m_h(height)
{
}


Bounds::~Bounds()
{
}


void Bounds::resize(int x, int y, int width, int height)
{
	m_x = x;
	m_y = y;
	m_w = width;
	m_h = height;
}


bool Bounds::contains(std::pair<int,int> &pos)
{
	bool flag = false;

	if (pos.first < this->m_w && pos.first > this->m_x && pos.second < this->m_h && pos.second > this->m_y)
	{
		flag = true;
	}

	return flag;
}


bool Bounds::overlap(Bounds &other)
{
	bool overlap = this->m_x > (other.getX() + other.getWidth()) ||
		(this->m_x + this->m_w) < other.getX() ||
		this->m_y > (other.getY() + other.getHeight()) ||
		(this->m_y + this->m_h) < other.getY();

	return !overlap;
}