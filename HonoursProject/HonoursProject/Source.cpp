#pragma once

#include "Application\Application.h"

int main(int argc, char *args[])
{
	Application app;

	app.initMaze(3, 220, 220, 20, 1);

	if (app.initSDL() == true)
	{
		app.update();
	}

	app.~Application();

	return 0;
}