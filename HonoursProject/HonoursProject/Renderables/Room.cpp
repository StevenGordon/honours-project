#include "Room.h"


Room::Room()
{
	m_rect = { 0, 0, 0, 0 };

	m_colour[0] = 0xFF;
	m_colour[1] = 0xFF;
	m_colour[2] = 0xFF;
	m_colour[3] = 0xFF;

	m_id = 0;

	bounds = Bounds(m_rect.x, m_rect.y, m_rect.w, m_rect.h);
}


Room::~Room()
{
}


void Room::copy(Room &room)
{
	this->m_rect.x = room.getPos().first;
	this->m_rect.y = room.getPos().second;
	this->m_rect.w = room.getSize().first;
	this->m_rect.h = room.getSize().second;

	this->m_id = room.getID();
}


void Room::init(int posx, int posy, int width, int height, int id, int type)
{
	m_rect = { posx, posy, width, height };

	m_id = id;

	bounds.resize(m_rect.x, m_rect.y, m_rect.w, m_rect.h);

	m_colour[0] = 0xFF;
	m_colour[1] = 0xFF;
	m_colour[2] = 0xFF;
	m_colour[3] = 0xFF;

}


void Room::recolour(int type)
{
	switch (type)
	{
	case 1:	// maze
		m_colour[0] = 0xFF;
		m_colour[1] = 0xFF;
		m_colour[2] = 0xFF;
		m_colour[3] = 0xFF;
		break;

	case 2:	// A* Path
		m_colour[0] = 0xFF;
		m_colour[1] = 0x00;
		m_colour[2] = 0x00;
		m_colour[3] = 0xFF;
		break;

	case 3:	// ACO Path
		m_colour[0] = 0x00;
		m_colour[1] = 0xFF;
		m_colour[2] = 0x00;
		m_colour[3] = 0xFF;
		break;

	case 4:	// PSO Path
		m_colour[0] = 0x00;
		m_colour[1] = 0xFF;
		m_colour[2] = 0xFF;
		m_colour[3] = 0xFF;
		break;

	default:
		m_colour[0] = 0x00;
		m_colour[1] = 0x00;
		m_colour[2] = 0x00;
		m_colour[3] = 0xFF;
		break;

	}
}


void Room::move(int newX, int newY)
{
	this->m_rect.x = newX;
	this->m_rect.y = newY;
}


void Room::render(SDL_Renderer *renderer)
{
	SDL_Rect rect = { m_rect.x*10, m_rect.y*10, m_rect.w*10, m_rect.h*10 };
	SDL_SetRenderDrawColor(renderer, m_colour[0], m_colour[1], m_colour[2], m_colour[3]);
	SDL_RenderFillRect(renderer, &rect);
}


float Room::distanceTo(Room other)
{
	/*
	int dx = bounds.getX() - other.bounds.getX();
	int dy = bounds.getY() - other.bounds.getY();

	return (float)SDL_sqrt(dx*dx + dy*dy);
	*/
	int thisX, thisY, otherX, otherY, distX, distY = 0;

	if (this->m_rect.x < other.m_rect.x)
	{
		thisX = this->m_rect.x + this->m_rect.w;
		otherX = other.m_rect.x;
	}
	else
	{
		otherX = other.m_rect.x + other.m_rect.w;
		thisX = this->m_rect.x;
	}

	if (this->m_rect.y < other.m_rect.y)
	{
		thisY = this->m_rect.y + this->m_rect.h;
		otherY = other.m_rect.y;
	}
	else
	{
		otherY = other.m_rect.y + other.m_rect.h;
		thisY = this->m_rect.y;
	}

	distX = abs(thisX - otherX);
	distY = abs(thisY - otherY);

	return ((float)distX < (float)distY ? (float)distX : (float)distY);
}


bool Room::overlap(Room other)
{
	return this->bounds.overlap(other.getBounds());
}


std::pair<int, int> Room::getPos()
{
	return std::make_pair(m_rect.x, m_rect.y);
}


std::pair<int, int> Room::getSize()
{
	return std::make_pair(m_rect.w, m_rect.h);
}
