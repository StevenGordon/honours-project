#pragma once

#include "../Bounds.h"

#include <utility>
#include <SDL.h>

class Room
{
public:
	Room();

	~Room();

	void copy(Room &room);

	void init(int posx, int posy, int width, int height, int id, int type);

	// 1 = no path, 2 = A*, 3 = ACO, 4 = PSO
	void recolour(int type);

	void move(int newX, int newY);

	void render(SDL_Renderer *renderer);

	float distanceTo(Room other);

	bool overlap(Room other);

	std::pair<int, int> getPos();
	std::pair<int, int> getSize();

	int getID() { return m_id; }

	Bounds getBounds() { return bounds; }

private:
	// Room Identifier
	int m_id;

	SDL_Rect m_rect;

	Uint8 m_colour[4];

	Bounds bounds;
};

